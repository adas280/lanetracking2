from Tkinter import Image

import cv2
import numpy as np

# img = 'roadtest1.png'

# 0 sets image to be read grayscale
# img = cv2.imread('Media/' + img, 0)
img = cv2.imread('Media/roadtest1cropped.png', 0)

# img = cv2.Canny(img, 0, 255, L2gradient=False)
# img = cv2.resize(img, (1000, 500))  # Resize image

kernel = np.ones((3, 3), np.uint8)
opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
# erosion = cv2.erode(img, kernel, iterations=3)

# ys between 0 and 300 -- make black
# for y in erosion:
#     for x in erosion:
#         erosion[x, y] = 0

cv2.imshow('image', img)
# cv2.destroyAllWindows()


threshImg = cv2.adaptiveThreshold(opening, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 33, -20)

# ret, th1 = cv2.threshold(img, 180, 255, cv2.THRESH_BINARY)
# cv2.imshow("threshold", th1)


# Read the image you want connected components of
# You need to choose 4 or 8 for connectivity type
connectivity = 4
# Perform the operation
output = cv2.connectedComponentsWithStats(threshImg, connectivity, cv2.CV_32S)
# Get the results
# The first cell is the number of labels
num_labels = output[0]
# The second cell is the label matrix
labels = output[1]
# The third cell is the stat matrix
stats = output[2]
# The fourth cell is the centroid matrix
centroids = output[3]

# print stats

for i in stats:
    # print i
    if 30 < i[2] < 40:
        img[threshImg == 255] = [0, 0, 255]
        # threshImg[i[1], i[0]] = [255, 0, 0]

cv2.imshow("adaptive threshold", threshImg)  # use over thresholding
# need wait key when using imshow
cv2.waitKey(0)
