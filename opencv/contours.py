import numpy as np
import cv2
import config


# TODO: Adapt to ignore parts of the image that are outside where the lane is expected to be
def validateBox(box, im):
    imHeight, imWidth = im.shape
    # box[0][0] is the x/width value
    if box[0][1] > imHeight * config.MAX_IMG_HEIGHT and box[0][0] > imWidth * config.MAX_IMG_WIDTH:
        return True
    return False


def calculateArea(rect):
    num = (rect[0][0] - rect[0][1]) * (rect[1][0] - rect[1][1])
    return abs(num)


cap = cv2.VideoCapture(config.VIDEO_FILE)

# Default resolutions of the frame are obtained.The default resolutions are system dependent.
# We convert the resolutions from float to integer.


frame_width = int(cap.get(3)) / (int(1 / config.RESIZE_VAL))
frame_height = int(cap.get(4)) / (int(1 / config.RESIZE_VAL))

# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
# TODO: What does the 10 mean?
out = cv2.VideoWriter('Media/outpy.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (frame_width, frame_height))

while cap.isOpened():
    ret, frame = cap.read()
    frame = cv2.resize(frame, (0, 0), fx=config.RESIZE_VAL, fy=config.RESIZE_VAL)
    imOrig = cv2.cvtColor(frame, cv2.IMREAD_COLOR)
    im = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel)

    # opening = cv2.dilate(opening, kernel, iterations=config.ITERATIONS)  # seems to work best with 2 iterations

    thresh = cv2.adaptiveThreshold(opening, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 33, -20)

    im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)

    relevantContours = []
    total = 0

    for i in contours:
        rect = cv2.minAreaRect(i)
        total += calculateArea(rect)
    averageArea = total / len(contours)

    for i in contours:
        rect = cv2.minAreaRect(i)
        # if the angle is between 0 and 60 degrees and area is greater than average area of all boxes, draw it
        if config.MIN_ANGLE < abs(rect[2]) < config.MAX_ANGLE and calculateArea(rect) > averageArea:
            box = cv2.boxPoints(rect)
            box = np.int0(box)

            if validateBox(box, im):
                relevantContours.append(box)
                thresh = cv2.drawContours(thresh, [box], 0, (0, 0, 255), 2)
                imOrig = cv2.drawContours(imOrig, [box], 0, (0, 0, 255), 2)

    cv2.imshow("thresh", thresh)
    cv2.imshow("im", imOrig)

    out.write(imOrig)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
out.release()
cv2.destroyAllWindows()
# getContours('Media/svroad.PNG')
