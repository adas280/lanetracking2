import numpy as np
import cv2
import config


# get all contours
# param: black/white frame
# return: contours: the array of all contours
#         thresh: binary image
def getContours(im):
    kernel = np.ones((3, 3), np.uint8)  # get kernel

    opening = cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel)  # apply opening operations (dilation/erosion)

    # might need to dilate/erode more if necessary
    thresh = cv2.adaptiveThreshold(opening, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 33, -20)

    multiple = 0.33
    step = 5
    for y in range(0, imHeight, step):
        for x in range(0, imWidth):
            if (y < imHeight * config.MAX_IMG_HEIGHT):
                thresh[y:y+step,x] = 0
            elif (x < multiple  * imWidth or x > imWidth - (multiple * imWidth)):
                thresh[y:y+step, x] = 100
                multiple -= 0.000020
    # thresh[0:int(config.MAX_IMG_HEIGHT * imHeight),
    # int(config.MIN_IMG_WIDTH * imWidth):int(config.MAX_IMG_WIDTH * imWidth)] = 0
    #
    # # right panel
    # thresh[0:, int(config.MAX_IMG_WIDTH * imWidth):] = 0
    #
    # # left panel
    # thresh[0:, 0:int(config.MIN_IMG_WIDTH * imWidth)] = 0
    # cv2.imshow("thresh 2", thresh)
    im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)  # converts image to gray but allows it to be in color

    return contours, thresh


# also eliminates contours that are too small to be relevant
def filterAnglesAndPos(contours, im):
    greatRects = []
    greatContours = []
    greatContoursRight = []
    greatContoursLeft = []
    for contour in contours:
        rect = cv2.minAreaRect(contour)
        if (config.MIN_ANGLE < abs(rect[2]) < config.MAX_ANGLE) and calculateArea(
                rect) > config.MIN_AREA:  # if between specified angles
            box = cv2.boxPoints(rect)
            box = np.int0(box)

            imHeight, imWidth = im.shape
            # box[0][0] is the x/width value

            # Get contours if on the left side of the image
            if box[0][1] > imHeight * config.MAX_IMG_HEIGHT and (
                    (1.0 * imWidth) / 2 > box[0][0] > imWidth * config.MIN_IMG_WIDTH):
                greatRects.append(rect)
                greatContours.append(contour)
                greatContoursLeft.append(contour)

            # Get contours if on the right side of the image
            if box[0][1] > imHeight * config.MAX_IMG_HEIGHT and (
                    (1.0 * imWidth) / 2 < box[0][0] < imWidth * config.MAX_IMG_WIDTH):
                greatRects.append(rect)
                greatContours.append(contour)
                greatContoursRight.append(contour)

    return greatRects, greatContours, greatContoursLeft, greatContoursRight


def calculateArea(rect):
    num = (rect[0][0] - rect[0][1]) * (rect[1][0] - rect[1][1])
    return abs(num)


def checkArea(greatContours):
    try:
        maxCnt = greatContours[0]
        for cnt in greatContours:
            if cv2.contourArea(cnt) > cv2.contourArea(maxCnt):
                maxCnt = cnt
        return maxCnt
    except IndexError:
        ()
    return []


def drawBoxes(im, finalRects):
    for rect in finalRects:
        box = cv2.boxPoints(rect)
        box = np.int0(box)

        im = cv2.drawContours(im, [box], 0, (0, 0, 255), 2)
        im = cv2.drawContours(im, [box], 0, (0, 0, 255), 2)

    return im


def checkSolidity(contours):
    excellentContours = []
    for cnt in contours:
        area = cv2.contourArea(cnt)
        hull = cv2.convexHull(cnt)
        hull_area = cv2.contourArea(hull)
        try:
            solidity = float(area) / hull_area
            if solidity > config.MIN_SOLIDITY:
                excellentContours.append(cnt)
        except ZeroDivisionError:
            ()
    return excellentContours


def applyMask(img, mask):
    maskedImage = []
    for x in range(0, imHeight, 1):
        for y in range(0, imWidth, 1):
            # print img[x][y].all()
            # print mask[x][y]
            return


# MAIN METHOD

cap = cv2.VideoCapture(config.VIDEO_FILE)

# Default resolutions of the frame are obtained.The default resolutions are system dependent.
# We convert the resolutions from float to integer.

frame_width = int(cap.get(3)) / (int(1 / config.RESIZE_VAL))
frame_height = int(cap.get(4)) / (int(1 / config.RESIZE_VAL))

out = cv2.VideoWriter(config.VIDEO_OUTPUT, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (frame_width, frame_height))

while cap.isOpened():

    ret, frame = cap.read()
    if ret:
        frame = cv2.resize(frame, (0, 0), fx=config.RESIZE_VAL, fy=config.RESIZE_VAL)
    else:
        exit(0)

    imOrig = cv2.cvtColor(frame, cv2.IMREAD_COLOR)

    im = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    imHeight, imWidth = im.shape

    # [y,x]

    contours, thresh = getContours(im)  # getting contours

    # for cont in contours:
    #     # cv2.drawContours(imOrig, [cnt], 0, 255, -1)
    #     cv2.drawContours(thresh, [cont], 0, 120, -1)

    cv2.rectangle(thresh, (int(imWidth * config.MIN_IMG_WIDTH), int(imHeight * config.MAX_IMG_HEIGHT)),
                  (int(imWidth * config.MAX_IMG_WIDTH), int(imHeight)), (244, 66, 223), 3)

    contours = checkSolidity(contours)

    greatRects, greatContours, greatContoursLeft, greatContoursRight = filterAnglesAndPos(contours,
                                                                                          im)  # filter by angles and position in frame -- returns rectangles
    finalContours = []

    if len(greatContoursLeft) > 0:
        finalContours.append(checkArea(greatContoursLeft))

    if len(greatContoursRight) > 0:
        finalContours.append(checkArea(greatContoursRight))

    if len(finalContours) > 0:
        for cnt in finalContours:
            cv2.drawContours(imOrig, [cnt], 0, 255, -1)
            cv2.drawContours(thresh, [cnt], 0, 255, -1)
            pixelpoints = np.transpose(np.nonzero(imOrig))
            # print "pixelpoints:"
            # print pixelpoints
            # pixelpoints = cv2.findNonZero(mask)

    cv2.rectangle(imOrig, (int(imWidth * config.MIN_IMG_WIDTH), int(imHeight * config.MAX_IMG_HEIGHT)),
                  (int(imWidth * config.MAX_IMG_WIDTH), int(imHeight)), (244, 66, 223), 3)


    cv2.imshow("Binary Image", thresh)
    cv2.imshow("Original Image", imOrig)
    # cv2.imshow("Mask", mask)

    out.write(thresh)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
out.release()
cv2.destroyAllWindows()
