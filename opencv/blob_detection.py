# Standard imports
import cv2
import numpy as np

# Read image
from py2app.recipes import scipy

im = cv2.imread('Media/test7.png', cv2.IMREAD_GRAYSCALE)

im = cv2.resize(im, (0, 0), fx=0.5, fy=0.5)

# Setup SimpleBlobDetector parameters.
params = cv2.SimpleBlobDetector_Params()

params.filterByArea = True
params.minArea = 400

# params.maxArea = 2000
params.filterByColor = True
params.blobColor = 255

params.filterByConvexity = False
params.filterByInertia = False

# Set up the detector with default parameters.
detector = cv2.SimpleBlobDetector_create(params)
# Filter by Area.

kernel = np.ones((3, 3), np.uint8)
opening = cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel)

threshImg = cv2.adaptiveThreshold(opening, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 33, -20)

# put a black border around the edge
for i in range(0, np.size(threshImg, 1)):
    for j in range(0, np.size(threshImg, 0) * 3 / 4):
        threshImg[j, i] = 0

# Detect blobs.
keypoints = detector.detect(threshImg)

for i in keypoints:
    print i.size
    print i.pt

# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
im_with_keypoints = cv2.drawKeypoints(threshImg, keypoints, np.array([]), (0, 0, 255),
                                      cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

cv2.imshow("threshImg", threshImg)

# imgFill = im_with_keypoints.copy()
# for x in range(1, len(keypoints)):
#     imgFill = cv2.circle(imgFill, (np.int(keypoints[x].pt[0]), np.int(keypoints[x].pt[1])),
#                          radius=np.int(keypoints[x].size), color=(255), thickness=-1)
#
# cv2.imshow("imgFill", imgFill)

# Show keypoints
cv2.imshow("Keypoints", im_with_keypoints)
cv2.waitKey(0)
