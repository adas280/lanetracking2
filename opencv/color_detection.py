import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread('Media/circles.png', 1)

hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

lower_range = np.array([99, 100, 100], dtype=np.uint8)
upper_range = np.array([189, 255, 255], dtype=np.uint8)

mask = cv2.inRange(hsv, lower_range, upper_range)

cv2.imshow('image', img)
cv2.imshow('mask', mask)

plt.subplot(121), plt.imshow(img)
plt.subplot(122), plt.imshow(mask)
plt.show()

# waiting for user to enter esc
while (1):
    k = cv2.waitKey(0)
    if k == 27:
        break

cv2.destroyAllWindows()