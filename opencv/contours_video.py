import cv2

# 'Media/20sec..mp4'

cap = cv2.VideoCapture('Media/20sec..mp4')

while True:
    ret, frame = cap.read()
    frame = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    getContours(frame)

    # Display the resulting frame
    cv2.imshow('frame', gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    # cv2.waitKey(0)

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

# getContours(frame)
