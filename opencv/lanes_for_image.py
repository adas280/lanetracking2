import numpy as np
import cv2


def validateBox(box):
    if box[0][1] > imHeight / 2 and box[0][0] > imWidth / 4:
        return True
    return False


def calculateArea(rect):
    num = (rect[0][0] - rect[0][1]) * (rect[1][0] - rect[1][1])
    return abs(num)


# Read image
imOrig = cv2.imread('Media/untitled.jpg', cv2.IMREAD_COLOR)
imOrig = cv2.resize(imOrig, (0, 0), fx=0.5, fy=0.5)

im = cv2.imread('Media/untitled.jpg', cv2.IMREAD_GRAYSCALE)
im = cv2.resize(im, (0, 0), fx=0.5, fy=0.5)

imHeight, imWidth = im.shape

kernel = np.ones((3, 3), np.uint8)
opening = cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel)

opening = cv2.dilate(opening, kernel, iterations=2)

thresh = cv2.adaptiveThreshold(opening, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 33, -20)

im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)

cnt = contours[3]

relevantContours = []
total = 0

for i in contours:
    rect = cv2.minAreaRect(i)
    total += calculateArea(rect)
averageArea = total / len(contours)

for i in contours:
    rect = cv2.minAreaRect(i)
    # if the angle is between 0 and 60 degrees and area is greater than average area of all boxes, draw it
    if 0 < abs(rect[2]) < 60 and calculateArea(rect) > averageArea:
        box = cv2.boxPoints(rect)
        box = np.int0(box)

        if validateBox(box):
            relevantContours.append(box)
            thresh = cv2.drawContours(thresh, [box], 0, (0, 0, 255), 2)
            imOrig = cv2.drawContours(imOrig, [box], 0, (0, 0, 255), 2)

cv2.imshow("thresh", thresh)
cv2.imshow("im", imOrig)

cv2.waitKey(0)